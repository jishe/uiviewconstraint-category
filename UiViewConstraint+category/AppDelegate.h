//
//  AppDelegate.h
//  UiViewConstraint+category
//
//  Created by 井泉 on 16/8/15.
//  Copyright © 2016年 whcl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

