//
//  ViewController.m
//  UiViewConstraint+category
//
//  Created by 井泉 on 16/8/15.
//  Copyright © 2016年 whcl. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Constraint.h"

@interface ViewController ()
{
    UIView *view1;
    UIView *view2;
}

@end

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIView *rootView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 200)];
    [self.view addSubview:rootView];
    rootView.backgroundColor = [UIColor blueColor];
    rootView.center = CGPointMake(self.view.center.x, self.view.center.y - 200);

    UIView *View1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View1];
    View1.backgroundColor = [UIColor redColor];
    View1.center = self.view.center;
    [View1 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideRightEdge offset:@[@1, @1]];

    UIView *View2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View2];
    View2.backgroundColor = [UIColor redColor];
    View2.center = self.view.center;
    [View2 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideLeftEdge offset:@[@1, @1]];
    
    UIView *View3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View3];
    View3.backgroundColor = [UIColor redColor];
    View3.center = self.view.center;
    [View3 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideTopEdge offset:@[@1, @1]];
    
    UIView *View4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View4];
    View4.backgroundColor = [UIColor redColor];
    View4.center = self.view.center;
    [View4 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideBottomEdge offset:@[@1, @1]];
    
    //外角
    UIView *View5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View5];
    View5.backgroundColor = [UIColor redColor];
    View5.center = self.view.center;
    [View5 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideRightEdgeTop offset:@[@1, @1]];
    
    UIView *View6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View6];
    View6.backgroundColor = [UIColor redColor];
    View6.center = self.view.center;
    [View6 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideRightEdgeBottom offset:@[@1, @1]];
    
    UIView *View7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View7];
    View7.backgroundColor = [UIColor redColor];
    View7.center = self.view.center;
    [View7 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideUpperEdgeLeft offset:@[@1, @1]];
    
    UIView *View8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View8];
    View8.backgroundColor = [UIColor redColor];
    View8.center = self.view.center;
    [View8 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideUpperEdgeRight offset:@[@1, @1]];
    
    UIView *View9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View9];
    View9.backgroundColor = [UIColor redColor];
    View9.center = self.view.center;
    [View9 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideLeftEdgeTop offset:@[@1, @1]];
    
    UIView *View10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View10];
    View10.backgroundColor = [UIColor redColor];
    View10.center = self.view.center;
    [View10 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideLeftEdgeBottom offset:@[@1, @1]];
    
    UIView *View11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View11];
    View11.backgroundColor = [UIColor redColor];
    View11.center = self.view.center;
    [View11 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideLowerEdgeLeft offset:@[@1, @1]];
    
    UIView *View12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View12];
    View12.backgroundColor = [UIColor redColor];
    View12.center = self.view.center;
    [View12 constraintStickyWithView:rootView orientation:constraintStickyTypeOutsideLowerEdgeRight offset:@[@1, @1]];
    
    //
    //
    //内部
    //
    //
    UIView *View13 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View13];
    View13.backgroundColor = [UIColor greenColor];
    View13.center = self.view.center;
    [View13 constraintStickyWithView:rootView orientation:constraintStickyTypeInsideRightEdge offset:@[@1, @1]];
    
    UIView *View14 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View14];
    View14.backgroundColor = [UIColor greenColor];
    View14.center = self.view.center;
    [View14 constraintStickyWithView:rootView orientation:constraintStickyTypeInsideLeftEdge offset:@[@1, @1]];
    
    UIView *View15 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View15];
    View15.backgroundColor = [UIColor greenColor];
    View15.center = self.view.center;
    [View15 constraintStickyWithView:rootView orientation:constraintStickyTypeInsideTopEdge offset:@[@1, @1]];
    
    UIView *View16 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View16];
    View16.backgroundColor = [UIColor greenColor];
    View16.center = self.view.center;
    [View16 constraintStickyWithView:rootView orientation:constraintStickyTypeInsideBottomEdge offset:@[@1, @1]];
    
    UIView *View17 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View17];
    View17.backgroundColor = [UIColor greenColor];
    View17.center = self.view.center;
    [View17 constraintStickyWithView:rootView orientation:constraintStickyTypeUpperRightCorner offset:@[@1, @1]];
    
    UIView *View18 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View18];
    View18.backgroundColor = [UIColor greenColor];
    View18.center = self.view.center;
    [View18 constraintStickyWithView:rootView orientation:constraintStickyTypeUpperLeftCorner offset:@[@1, @1]];

    UIView *View19 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View19];
    View19.backgroundColor = [UIColor greenColor];
    View19.center = self.view.center;
    [View19 constraintStickyWithView:rootView orientation:constraintStickyTypeLowerLeftCorner offset:@[@1, @1]];
    
    UIView *View20 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View20];
    View20.backgroundColor = [UIColor greenColor];
    View20.center = self.view.center;
    [View20 constraintStickyWithView:rootView orientation:constraintStickyTypeLowerRightCorner offset:@[@1, @1]];
    
    //两点Lable
    UILabel *myLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    myLable.backgroundColor = [UIColor grayColor];
    myLable.text = @"我的Lable";
    myLable.textColor = [UIColor blackColor];
    [self.view addSubview:myLable];
    myLable.center = CGPointMake(self.view.center.x, self.view.bounds.size.height - 50);
    
    UILabel *myLable1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    myLable1.backgroundColor = [UIColor grayColor];
    myLable1.text = @"我的Lable";
    myLable1.textColor = [UIColor blackColor];
    [self.view addSubview:myLable1];
    myLable1.center = CGPointMake(self.view.center.x - 150, self.view.center.y + 50);
    
    UILabel *myLable2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    myLable2.backgroundColor = [UIColor grayColor];
    myLable2.text = @"我的Lable";
    myLable2.textColor = [UIColor blackColor];
    [self.view addSubview:myLable2];
    myLable2.center = CGPointMake(self.view.center.x + 150, self.view.center.y + 50);

    //
    //两点约束
    //
    UIView *View21 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    [self.view addSubview:View21];
    View21.backgroundColor = [UIColor greenColor];
    View21.center = self.view.center;
    [View21 constraint2View:@[rootView, self.view] constraintType:constraint2ViewTypeRightEdge offset:@[@1, @1]];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
