//
//  main.m
//  UiViewConstraint+category
//
//  Created by 井泉 on 16/8/15.
//  Copyright © 2016年 whcl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
